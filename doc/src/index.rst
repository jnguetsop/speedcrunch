.. SpeedCrunch documentation master file, created by
   sphinx-quickstart on Mon Feb  8 20:22:01 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. This explicit qtkeyword directive serves to identify the "home" location:
.. when just opening the manual, this is what is displayed.

.. qtkeyword:: id="home"

Welcome to SpeedCrunch's documentation!
=======================================
    
SpeedCrunch is a high-precision scientific desktop calculator. It features a syntax-highlighted scrollable display and is designed to be fully used via keyboard.
Some distinctive features are auto-completion of functions and variables, a formula book, and quick insertion of constants from various fields of knowledge.
It is available for Windows, OS X, and Linux in a number of languages.

Have fun with SpeedCrunch!


Table of Contents
=================

.. toctree::
   :maxdepth: 2

   userguide/index
   reference/index
   advanced/index


Indices and tables
==================

.. Checking for the sc_bundled_docs tag here so as to not include a link to the search
.. page when building for bundling.

.. only:: sc_bundled_docs

   * :ref:`genindex`
   * :ref:`sc:functionindex`

.. only:: not sc_bundled_docs

   * :ref:`genindex`
   * :ref:`sc:functionindex`
   * :ref:`search`
